const assert = require('assert')

function getTarget(input: { target: number; inputs: number[] }) {
    let resp: Array<number> = []
    let stop = false
    const { target, inputs } = input
    for (let index = 0; index < inputs.length && stop === false; index++) {
        const input = inputs[index]

        inputs.some((s, i) => {
            if (index != i && input + s === target) {
                resp.push(index)
                resp.push(i)
                stop = true
                return true
            }
        })
    }
    return resp
}

// [2,7,11,15], target 9 | 0,1
// [3,2,4], target = 6 | 1,2
// [3,3], target = 6 | 0,1
// [1,3,0,7,5], target = 8 | 0,3

describe('Test of array number and targe', () => {
    it('Target 8 and the array is [1, 3, 0, 7, 5], response should be 0,3', () => {
        const resp = getTarget({
            target: 8,
            inputs: [1, 3, 0, 7, 5],
        })
        assert.equal(JSON.stringify(resp), JSON.stringify([0, 3]))
    })
})
